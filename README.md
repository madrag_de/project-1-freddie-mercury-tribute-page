This is the first responsive web design project for freeCodeCamp.org.

The project was to create a basic tribute page, and the project requirements can be found [here](https://learn.freecodecamp.org/responsive-web-design/responsive-web-design-projects/build-a-tribute-page).